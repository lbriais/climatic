CLImatic
========

Basically `CLImatic` is the successor of [easy_app_helper]. Most of the documentation in [easy_app_helper] remains valid, but `CLImatic` introduces:

- sub-commands _à-la-git_
- a brand new mechanism so that command line options can be defined using YAML files (see the [YAML example]).
- command line definition could be contextual to the folder you're in, thanks to a new layer introduced compared to [easy_app_helper], the [ProjectLayer].


## Installation

Add this line to your application's Gemfile:

```ruby
gem 'climatic'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install climatic

## Usage

Specific documentation will come, for the time being I'am afraid you may have to read the code and more specifically the tests...

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/lbriais/climatic. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Climatic project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/lbriais/climatic/blob/master/CODE_OF_CONDUCT.md).

[easy_app_helper]: https://github.com/lbriais/easy_app_helper "Could be seen as the legacy Climatic"
[YAML example]: example/simple_app/etc/command_line.yml "A YAML example to define command line"
[example application]: example/simple_app "An example application using Climatic"
[ProjectLayer]: lib/climatic/config_layers/project_layer.rb "A layer to have the command options diffrent if you are within a specific project"