module Climatic

  module Initializer

    include Climatic::Utils::ScriptHelper

    attr_reader :config

    def bootstrap(cmd_line_args: ARGV.dup,
                  command_manager: Climatic::ConfigLayers::CommandLineLayer.default_command_line_manager)
      raise Climatic::Error, 'You cannot bootstrap Climatic framework twice !' if climatic_bootstrapped?
      @climatic_status = :bootstrapping
      setup_initial_logger
      Climatic.logger.debug 'Starting Climatic framework setup...'
      # Get the config first to correctly setup the definitive logger
      setup_config_manager cmd_line_args, command_manager
      # Now we can setup the definitive logger
      setup_logger
      Climatic.logger.debug 'Climatic framework setup complete.'
      @climatic_status = :bootstrapped
    rescue Slop::UnknownOption
      Climatic.logger.debug 'Climatic initialization failed (wrong command line options) !'
    end

    def climatic_bootstrapping?
      @climatic_status == :bootstrapping
    end

    def climatic_bootstrapped?
      @climatic_status == :bootstrapped
    end

    private

    def setup_config_manager(cmd_line_args, command_manager)
      @config = Climatic::LayersManager.new
      config.command_line_layer.command_line_manager = command_manager
      config.command_line_layer.cmd_line_args = cmd_line_args
    end

    def setup_initial_logger
      @logger = Climatic::Logger::Accumulator.new
      UltraCommandLine.logger = @logger
    end

    def setup_logger
      if config[:debug]
        log_device = if config[:'log-file']
                       if File.exists? config[:'log-file']
                         if File.writable? config[:'log-file']
                           if config[:'truncate-log-file']
                             File.open(config[:'log-file'], 'w') do |log_file|
                               log_file.puts "Log truncated on #{Time.now}"
                             end
                           end
                           config[:'log-file']
                         else
                           STDERR.puts "WARNING: Log file '#{config[:'log-file']}' is not writable. Switching to STDERR..."
                           config[:'log-file'] = nil
                           STDERR
                         end
                       else
                         if File.writable? File.dirname(config[:'log-file'])
                           config[:'log-file']
                         else
                           STDERR.puts "WARNING: Cannot write log file in '#{File.dirname config[:'log-file']}'. Switching to STDERR..."
                           config[:'log-file'] = nil
                           STDERR
                         end
                       end
                     elsif config[:'debug-on-stderr']
                       STDERR
                     else
                       STDOUT
                     end

        new_logger = ::Logger.new log_device
        new_logger.level = config[:'log-level'] || Climatic::Logger::Manager::DEFAULT_LOG_LEVEL
        self.logger = new_logger
      else
        self.logger = user_defined_logger
      end
    end

  end

end