module Climatic
  module ConfigLayers

    class ProvidedConfigFileLayer < SuperStack::Layer


      def managed
        if manager[:'config-file']
          if File.readable? manager[:'config-file']
            @file_name = manager[:'config-file']
            self.merge_policy = SuperStack::MergePolicies::OverridePolicy if manager[:'config-override']
          end
        end
      end

      private

      def config_filepath
        File.expand_path File.join('..', '..', '..', '..', 'etc', 'default_cmd_line_options.yml'), __FILE__
      end


    end

  end
end
