module Climatic
  module ConfigLayers

    module CommandLineManagerBinder

      SUB_COMMANDS_HELP_HEADER = [
          '',
          '-' * 80,
          ' Following subcommands exist too:',
          ' For more information you can always issue sub_command_name --help...',
          '-' * 80
      ]

      attr_accessor :command_line_manager

      def cmd_line_args
        command_line_manager.cmd_line_args
      end

      def cmd_line_args=(args)
        command_line_manager.cmd_line_args = args
        reload
      end

      def extra_parameters
        with_selected_command {|c| c.extra_arguments }
      end

      def load
        with_selected_command do |c|
          replace c.params_hash
          @file_name = :none
        end
        self
      end
      alias reload load

      def help
        with_selected_command do |c|
          if c.root_command?
            res = c.help
            sub_commands = command_line_manager.commands.select {|command| not command.root_command?}
            unless sub_commands.empty?
              res.concat SUB_COMMANDS_HELP_HEADER
              sub_commands.sort{|a,b| a.name <=> b.name }.each do |command|
                # command_summary = "#{command.help[0][0,60].tr("\n", ' ')}..."
                res << " * #{command.name}: #{command_summary command}"
              end

            end
            res
          else
            c.help
          end
        end
      end

      private

      def command_summary(command)
        text = command.help[0][0,60].tr("\n", ' ')
        text.match(/^\s*(?<sentence>[^\s][^\.]+)\./) do |md|
          return "#{md['sentence']} ..."
        end
        text
      end

      def with_selected_command(&block)
        selected_command = nil
        begin
          selected_command = command_line_manager.command
        rescue UltraCommandLine::Error => e
          UltraCommandLine.logger.debug 'No valid command found !'
        end
        unless selected_command.nil?
          block.call selected_command
        end
      end


    end

  end
end