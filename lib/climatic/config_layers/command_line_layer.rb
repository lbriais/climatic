require 'climatic/config_layers/command_line_manager_binder'

module Climatic
  module ConfigLayers

    class CommandLineLayer < SuperStack::Layer

      include Climatic::ConfigLayers::CommandLineManagerBinder

      CONFIG_ROOT_DIR = %w(etc config).freeze
      DEFAULT_COMMAND_LINE_DEFINITION_FILE = 'command_line.yml'.freeze
      COMMAND_LINE_MANAGER_CLASS = UltraCommandLine::Manager::Base

      def initialize(command_line_manager = self.class.default_command_line_manager)
        # raise UltraCommandLine::Error, 'Invalid command line manager !' unless command_line_manager.is_a? MANAGER_CLASS
        @command_line_manager = command_line_manager
      end

      private

      def self.default_command_line_manager
        begin
          CONFIG_ROOT_DIR.each do |sub_dir|
            default_definition_file = File.join Climatic::ConfigLayers::ExecutableGemLayer.executable_gem_config_root, sub_dir, DEFAULT_COMMAND_LINE_DEFINITION_FILE
            if File.readable? default_definition_file
              return build_command_line_manager default_definition_file
            end
          end
        rescue => e
          Climatic.logger.debug "#{e.message}\nBacktrace:\n#{e.backtrace.join("\n\t")}"
        end
        UltraCommandLine::Manager::Base.new
      end


      def self.build_command_line_manager(definition_file)
        mngr = COMMAND_LINE_MANAGER_CLASS.from_yaml_file definition_file
        mngr.definition_hash_to_commands
        mngr
      end


    end

  end
end
