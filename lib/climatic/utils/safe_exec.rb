module Climatic
  module Utils

    module SafeExec

      def safely_exec_code(*args, message: nil, &block)
        if Climatic.config[:simulate]
          Climatic.logger.puts_and_logs "[SIMULATION MODE]: #{message}" unless message.nil?
        else
          Climatic.logger.puts_and_logs message
          block.call *args
        end
      end

      def safely_exec_command(command, message: nil, show_output: false, log_output: true)
        safely_exec_code command, message: message do |cmd|
          process = Climatic::Processes::Base.new cmd
          process.show_output = show_output
          process.log_output = log_output
          process.execute
          process
        end
      end

    end

  end
end