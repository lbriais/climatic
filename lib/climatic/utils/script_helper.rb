module Climatic
  module Utils

    module ScriptHelper

      def display_exit_error(e)
        puts "Program aborted with message: '#{e.message}'."
        if Climatic.config[:debug]
          Climatic.logger.fatal "#{e.message}\nBacktrace:\n#{e.backtrace.join("\n\t")}"
        else
          puts '  Use --debug option for more detail (see --help).'
        end
      end

    end

  end
end
