module Climatic
  module Logger

    class Accumulator

      STACK_OPS = %i(debug info warn error fatal unknown).freeze

      attr_accessor :level

      def initialize
        @log_lines = [{op: :debug, args: ['Starting special temporary "accumulator" logger...']}]
      end

      def stack(op, *args)
        log_lines << {op: op, args: args}
      end

      def transfer_content_to(other_logger)
        debug "Transferring accumulated logs to logger '#{other_logger.inspect}'"
        if other_logger.nil?
          @log_lines = []
          return
        end
        log_lines.each do |log_line|
          other_logger.send log_line[:op], *log_line[:args]
        end
        @log_lines = []
      end

      def method_missing(method_name, *args)
        if STACK_OPS.include? method_name
          stack method_name, *args
        else
          super
        end
      end

      def respond_to_missing?(method_name, include_private = false)
        STACK_OPS.include?(method_name) || super
      end

      private

      attr_reader :log_lines

    end

  end
end
