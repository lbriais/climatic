module Climatic
  module Logger

    module Wrapper
      def puts_and_logs(*args, **options)
        check_verbose = options.fetch :check_verbose, true
        logs_as = options.fetch :logs_as, :info

        if check_verbose
          puts *args if Climatic.config[:verbose]
        else
          puts *args
        end
        self.send logs_as, *args
      end
    end

  end
end
