require 'logger'


module Climatic
  module Logger

    module Manager

      attr_reader :logger

      def puts_and_logs(*args, **options)
        logger.puts_and_logs *args, **options
      end

      def logger=(new_logger)

        new_logger ||= DEVNULL_LOGGER

        unless climatic_bootstrapped? or climatic_bootstrapping?
          @user_defined_logger = new_logger
          return
        end

        if climatic_bootstrapped?
          new_logger.level = config[:'log-level'].nil? ? Climatic::Logger::Manager::DEFAULT_LOG_LEVEL : config[:'log-level']
        end

        new_logger.extend Climatic::Logger::Wrapper

        if self.logger.respond_to? :transfer_content_to
          self.logger.transfer_content_to new_logger
        end

        UltraCommandLine.logger = new_logger
        @logger = new_logger
      end

      protected

      attr_reader :user_defined_logger

      private

      DEFAULT_LOG_LEVEL = ::Logger::Severity::WARN
      DEVNULL_LOGGER = UltraCommandLine::Utils::BasicLogger::NullLogger.new

    end

  end
end
