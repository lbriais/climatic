module Climatic

  module Script

    class Simple

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Script::Base

      def initialize
        register_processor
      end

      def register_processor(commands = cmd_line_mngr.commands, processor = self)
        commands = [commands] unless commands.is_a? Array
        commands.each do |command|
          cmd_line_mngr.register_processor command, processor
        end
      end

      private

      def cmd_line_mngr
        Climatic.config.command_line_layer.command_line_manager
      end

    end

  end
end

