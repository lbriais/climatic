module Climatic
  module Script

    module Base

      include Climatic::Utils::ScriptHelper

      module ClassMethods

        def start
          script = new
          script.run
        end

      end

      def run
        # logging startup configuration
        Climatic.config.command_line_layer.cmd_line_args = ARGV.dup
        Climatic.logger.debug "Config layers ->\n#{Climatic.config.detailed_layers_info}"
        Climatic.logger.debug "Merged config -> #{Climatic.config[].to_yaml}"
        # Displaying (and exiting) command line help
        display_help_and_exit if Climatic.config[:help]
        check_config
        Climatic.logger.info 'Application is starting...'
        do_process
        Climatic.logger.info 'Application ended normally...'
      rescue => e
        display_exit_error e
        exit_code = e.respond_to?(:exit_code) ? e.exit_code : 1
        exit exit_code
      ensure
        Climatic.logger.info 'Exiting...'
      end

      def self.included(base)
        base.extend ClassMethods
      end

      private

      def display_help_and_exit
        puts Climatic.config.command_line_help
        exit 0
      end

      def do_process
        cmd_line_mngr.processor.execute
      end

      def cmd_line_mngr
        Climatic.config.command_line_layer.command_line_manager
      end

      def check_config
        # Check options validity in terms of dependencies
        cmd_line_mngr.command.valid? raise_error: true
        # Delegates to the processor the functional checks of the config
        cmd_line_mngr.processor.check_params cmd_line_mngr.cmd_line_args_for_command(cmd_line_mngr.command)
      end

    end

  end
end