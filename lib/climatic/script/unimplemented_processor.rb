module Climatic

  module Script

    module UnimplementedProcessor

      def check_params(command_args)
        true
      end

      def execute
        raise Climatic::Error, 'Not yet implemented !'
      end

    end

  end
end
