module Climatic

  module Proxy

    def config
      Climatic.config
    end

    def logger
      Climatic.logger
    end

    def command_line_manager
      config.command_line_layer.command_line_manager
    end

    def help
      config.command_line_layer.help
    end

    def puts_and_logs(*args, **options)
      Climatic.logger.puts_and_logs *args, **options
    end

  end

end
