require 'climatic/version'

require 'super_stack'
require 'ultra_command_line'

require 'climatic/logger/accumulator'
require 'climatic/logger/wrapper'
require 'climatic/logger/manager'

require 'climatic/utils/error'
require 'climatic/utils/safe_exec'
require 'climatic/utils/script_helper'
require 'climatic/utils/input'
require 'climatic/config_layers/source_helper'
require 'climatic/config_layers/generic_layer'
require 'climatic/config_layers/system_layer'
require 'climatic/config_layers/executable_gem_layer'
require 'climatic/config_layers/gem_layer'
require 'climatic/config_layers/global_layer'
require 'climatic/config_layers/user_layer'
require 'climatic/config_layers/env_layer'
require 'climatic/config_layers/provided_config_file_layer'
require 'climatic/config_layers/project_layer'
require 'climatic/config_layers/command_line_layer'
require 'climatic/config_layers/program_description_helper'

require 'climatic/processes/time_management'
require 'climatic/processes/synchronous'
require 'climatic/processes/command'
require 'climatic/processes/base'

require 'climatic/layers_manager'


require 'climatic/initializer'
require 'climatic/proxy'

module Climatic
  extend Climatic::Logger::Manager
  extend Climatic::Utils::SafeExec
  extend Climatic::Initializer
end

# Let's kick off
Climatic.bootstrap unless $DO_NOT_AUTOSTART_CLIMATIC