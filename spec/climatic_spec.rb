require 'spec_helper'

describe Climatic do

  subject {described_class}


  it 'has a version number' do
    expect(subject::VERSION).not_to be nil
  end

  it 'should have a config' do
    expect(subject.config).to be_a Climatic::LayersManager
    expect(subject.config[]).to be_a_kind_of Hash
  end

  it 'should have a logger' do
    expect { subject.logger.fatal 'YO' }.not_to raise_error
    expect { subject.logger.debug 'BIM' }.not_to raise_error
    expect { subject.logger.unknown 'OOPS' }.not_to raise_error
  end


end
