require 'spec_helper'

class FakeLogger
  %i(debug info warn error fatal).each do |method_name|
    self.class_eval do
      define_method method_name do |*args|
        return [method_name, *args]
      end
    end
  end
end


describe Climatic::Logger::Wrapper do
  subject do
    s = FakeLogger.new
    s.extend described_class
    s
  end

  context 'when using puts_and_logs' do

    it 'should report as info by default' do
      expect(subject.puts_and_logs('test').first).to eq :info
    end

    it 'should be possible to log messages at any level' do
      %i(debug info warn error fatal).each do |level|
        expect(subject.puts_and_logs('test', logs_as: level).first).to eq level
      end
    end

  end

end
