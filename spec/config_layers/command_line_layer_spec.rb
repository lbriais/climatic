require 'spec_helper'


describe Climatic::ConfigLayers::CommandLineLayer do

  # subject { described_class.new test_cmdline_manager }

  context 'when command line parameter are passed' do

    # let(:test_cmdline_manager) do
    #   conf_file = File.expand_path File.join('..', '..', '..', 'test', 'test_default_options.yml'), __FILE__
    #   UltraCommandLine::Manager::Base.from_yaml_file conf_file
    # end

    let(:cmd_line) { ['--help'] }

    subject do
      conf_file = File.expand_path File.join('..', '..', '..', 'test', 'test_default_options.yml'), __FILE__
      test_cmdline_manager = UltraCommandLine::Manager::Base.from_yaml_file conf_file
      s = described_class.new test_cmdline_manager
      s.cmd_line_args = cmd_line
      test_cmdline_manager.command.params_hash
      s
    end

    it 'should end-up as a value in the layer' do
      expect(subject).not_to be_empty
      expect(subject[:help]).to be_truthy
    end

    context 'when extra command line parameter are passed (not part of a defined option)' do
      let(:extra_options)  {%w(extra1 extra2) }
      let(:cmd_line) { %w(--help  --verbose) + extra_options }

      it 'should provide the extra options while leaving ARGV untouched' do
        expect(subject.extra_parameters).to eq extra_options
        expect(subject[:help]).to be_truthy
      end
    end
  end




end